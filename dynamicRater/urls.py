"""
URL configuration for dynamicRater project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from raterapp import views as raterapp
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', raterapp.home, name="home"),
    path('qr/', raterapp.qr, name="qr"),
    path('exp/', raterapp.exp, name="exp"),
    path('end/', raterapp.end, name="end"),
    path('_getQrAnswers/', csrf_exempt(raterapp.getQrAnswers)),
    path('_getDynamicRating/', csrf_exempt(raterapp.getDynamicRating)),
    path('_validateExperiment/', csrf_exempt(raterapp.validateExperiment)),
    path('_testVoiceMorphing/', raterapp.testVoiceMorphing),
]
