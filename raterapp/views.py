from django.shortcuts import render
from django.http import JsonResponse
from user_agents import parse # pip install pyyaml ua-parser user-agents
import json, datetime, os, random



# CUSTOMIZE VARIABLES HERE
segmentsFolder = "static/audio/"   # Folder with all audio files to choose from randomly
trainingSegmentsFolder = "static/audioTraining/"   # Folder with audio files for training (not used in the experiment)
outputFolder = "output/"        # Output folder to export results

default_numberOfSegments = len(os.listdir(segmentsFolder))

numberOfSegments = default_numberOfSegments    # Number of audio to be randomly presented to each participant (default all the audio in segmentsFolder)
###

def home(request):
    raterID = ""
    return render(request, 'home.html', {"raterID":raterID})


def qr(request):
    raterID = ""
    return render(request, 'qr.html', {"raterID":raterID})


def exp(request):
    segments = random.sample(os.listdir(segmentsFolder), k=numberOfSegments)
    print(segments)

    trainingSegments = os.listdir(trainingSegmentsFolder)

    data = {
        "segments": json.dumps(segments),
        "trainingSegments": json.dumps(trainingSegments),
        "segmentsFolder": '/'+segmentsFolder,
        "trainingSegmentsFolder": '/'+trainingSegmentsFolder
    }

    print(data)

    return render(request, 'exp.html', data)

def testVoiceMorphing(request):
    # test view with specific audio file
    segments = []
    #trainingSegments = ['jan2023-503_109-064_SPEAKER_01_4_03_W-M_noise.wav']
    trainingSegments = [request.GET.get('file', None)]
    print("FILE:",trainingSegments)
    data = {
        "segments": json.dumps(segments),
        "trainingSegments": json.dumps(trainingSegments),
        "segmentsFolder": '/'+segmentsFolder,
        "trainingSegmentsFolder": '/static/audioTest/'
    }
    return render(request, 'exp.html', data)


def end(request):
    return render(request, 'end.html')



def getQrAnswers(request):
    ## Réception du colis
    colis = json.loads(request.body)
    prolificPid = colis['prolificPid']
    motherTongue = colis['motherTongue'].replace(';',',')
    country = colis['country']
    learntLang = colis['learntLang'].replace(';',',')
    userAgent = parse(request.META['HTTP_USER_AGENT'])
    
    timeStamp = datetime.datetime.now()

    print('Reception Qr answers from', prolificPid)
    print(colis)

    if 'questionnaires.csv' not in os.listdir(outputFolder):
        with open(outputFolder+'questionnaires.csv','a') as outf:
            outf.write(';'.join([ format(x) for x in [
                "prolificPid",
                "motherTongue",
                "country",
                "learntLang",
                "timeStamp",
                "userAgent"
            ]])+'\n')

    with open(outputFolder+'questionnaires.csv','a') as outf:
        outf.write(';'.join([ format(x) for x in [
            prolificPid,
            motherTongue,
            country,
            learntLang,
            timeStamp,
            userAgent
            ]])+'\n')
    
    ## Envoi réponse au client
    rep = {
        "rep":"OK",
    }
    
    return JsonResponse(rep)

def getDynamicRating(request):
    colis = json.loads(request.body)
    prolificPid = colis['prolificPid']
    segment = colis['segment']
    trainingPhase = colis['trainingPhase']
    clicks = colis['clicks']
    globalPron = colis['globalPron']
    globalFluency = colis['globalFluency']
    globalComp = colis['globalComp']
    globalComment = colis['globalComment'].replace(';',',')
    userAgent = parse(request.META['HTTP_USER_AGENT'])
    
    timeStamp = datetime.datetime.now()

    print('Reception dynamic rating from', prolificPid)
    print(colis)

    if 'dynamicRatings.csv' not in os.listdir(outputFolder):
        with open(outputFolder+'dynamicRatings.csv','a') as outf:
            outf.write(';'.join([ format(x) for x in [
                "prolificPid",
                "segment",
                "trainingPhase",
                "nbClicks",
                "clicks",
                "globalPron",
                "globalFluency",
                "globalComp",
                "globalComment",
                "timeStamp",
                "userAgent"
            ]])+'\n')

    with open(outputFolder+'dynamicRatings.csv','a') as outf:
        outf.write(';'.join([ format(x) for x in [
            prolificPid,
            segment,
            trainingPhase,
            len(clicks),
            clicks,
            globalPron,
            globalFluency,
            globalComp,
            globalComment,
            timeStamp,
            userAgent
        ]])+'\n')
    
    ## Envoi réponse au client
    rep = {
        "rep":"OK",
    }
    
    return JsonResponse(rep)


def validateExperiment(request):
    colis = json.loads(request.body)
    prolificPid = colis['prolificPid']
    comment = colis['comment'].replace(';',',')
    timeStamp = datetime.datetime.now()
    userAgent = parse(request.META['HTTP_USER_AGENT'])

    print('Reception dynamic rating from', prolificPid)
    print(colis)

    if 'finalComment.csv' not in os.listdir(outputFolder):
        with open(outputFolder+'finalComment.csv','a') as outf:
            outf.write(';'.join([ format(x) for x in [
                "prolificPid",
                "comment",
                "timeStamp",
                "userAgent"
            ]])+'\n')

    with open(outputFolder+'finalComment.csv','a') as outf:
        outf.write(';'.join([ format(x) for x in [
            prolificPid,
            comment,
            timeStamp,
            userAgent
        ]])+'\n')

    ## Envoi réponse au client
    rep = {
        "rep":"OK",
        "completionUrl":"https://app.prolific.com/submissions/complete?cc=CZRIC1X6"
    }
    
    return JsonResponse(rep)