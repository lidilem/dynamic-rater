###############
#
# dynamicRater2clickTables.py
#
# Generates a csv file for each audio, with each <time_step> in columns (default 1 second) and each rater in rows, with a supplementary row with all the raters' clicks
# each cell contains the sum of clicks within the given time_step for the given rater.
# 
# Arguments :
    # 1. csv file from the dynamicRater software (for each audio and rater, list of click timing)
    # 2. path to folder with the audio files 
    # 3. path to folder for the generated TextGrid files
    # 4. time step (default 1 second)
#
# S. Coulange 2024

import re, sys, os, json, wave, contextlib

input_file = sys.argv[1]
audio_folder = sys.argv[2]
if not audio_folder.endswith('/'): audio_folder += '/'
output_folder = sys.argv[3]
if not output_folder.endswith('/'): output_folder += '/'

time_step = 1 
if len(sys.argv)==5:
    time_step = sys.argv[4]
print('Time step set to', time_step, 'second(s)')


##################################################
#
# 1. Reading ratings file
#
print("Reading ratings file...")

file2records = {}

with open(input_file, "r") as inf:
    inf.readline()
    for cpt, line in enumerate(inf):
        line = line.strip()
        l = line.split(';')
        if len(l) != 11:
            print("NUMBER OF COLUMNS DOES NOT MATCH EXPECTATION! line", cpt)
            print(line)
        else:
            prolificPid, segment, trainingPhase, nbClicks, clicks, globalPron, globalFluency, globalComp, globalComment, timeStamp, userAgent = l
            segment = re.sub(r'(.mp3|.ogg|.wav)','',segment)

            if segment not in file2records.keys():
                file2records[segment] = []

            file2records[segment].append({
                "prolificPid":prolificPid,
                "segment":segment,
                "trainingPhase":trainingPhase,
                "nbClicks":int(nbClicks),
                "clicks":json.loads(clicks),
                "globalPron":int(globalPron),
                "globalFluency":int(globalFluency),
                "globalComp":int(globalComp),
                "globalComment":globalComment,
                "timeStamp":timeStamp,
                "userAgent":userAgent
                })

print('List of audio recordings with number of ratings:')
for seg, entries in file2records.items():
    print(seg, len(entries))


##################################################
#
# 2. Creating CSV files
#
print("Creating CSV files...")

for seg, entries in file2records.items():

    # Get audio duration
    #print("Get audio duration of",audio_folder + seg+'.wav ...')
    if seg+".wav" not in os.listdir(audio_folder):
        continue

    with contextlib.closing(wave.open(audio_folder + seg + ".wav",'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)

    table = [ { "xmin":x/1000, "xmax":x/1000+time_step, "ratings":{} } for x in range(0,round(duration*1000),time_step*1000) ] # This generates an empty table with 1 col per time_step for the audio duration
    # in each cell, we have got beginning time (xmin), ending time (xmax) and a dict (ratings) which will contain all the raters and their sum of click within this frame.

    raters = []
    allClicks = [] # all the clicks for this audio

    # LOOP ON EACH RATING FOR THE CURRENT AUDIO SEGMENT
    for entrie in sorted(entries,key=lambda t:t["prolificPid"]):
        # DO NOT allow multiple ratings for the same rater: only the first entry for a specific rater and a specific audio is kept (sometimes they confirm several times because of late server response, ending up to duplicate lines)
        currentRaterId = str(entrie['prolificPid'])
        if currentRaterId not in raters:
            raters.append(currentRaterId)
        else:
            continue

        # Init the rater row in all the frames of the table
        for frame in table:
            frame["ratings"][currentRaterId] = 0

        # Then increment the nb of clicks when necessary
        for i, click in enumerate(entrie["clicks"]):
            click = float(click)
            allClicks.append(click)

            for frame in table:
                if click >= frame["xmin"] and click < frame["xmax"]:
                    frame["ratings"][currentRaterId] += 1
            

    # Init the last row (all raters at once) in all the frames of the table
    for frame in table:
        frame["ratings"]["all"] = 0

    # Add all clicks in the last row
    for i, click in enumerate(sorted(allClicks)):
        for frame in table:
            if click >= frame["xmin"] and click < frame["xmax"]:
                frame["ratings"]["all"] += 1


    # Export:
    print("Exporting CSV file...", output_folder + seg + '.csv')
    with open(output_folder + seg + '.csv', 'w') as outf:
        outf.write(f'rater;{ ";".join( [ str(x["xmin"]) for x in table ]) }\n')

        for rater in raters:
            outf.write(f'{rater};{ ";".join( [ str(x["ratings"][rater]) for x in table ]) }\n')

        outf.write(f'all;{ ";".join( [ str(x["ratings"]["all"]) for x in table ]) }\n')

print("Done.")
