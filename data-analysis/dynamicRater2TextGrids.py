###############
#
# dynamicRater2TextGrids.py
#
# Generates a TextGrid file for each audio file, with one point tier per rater and points for each click. There is also a supplementary tier containing all the points.
# 
# Arguments :
    # 1. csv file from the dynamicRater software (for each audio and rater, list of click timing)
    # 2. path to folder with the audio files 
    # 3. path to folder for the generated TextGrid files
#
# S. Coulange 2024

import re, sys, os, textgrids, json, wave, contextlib

input_file = sys.argv[1]
audio_folder = sys.argv[2]
if not audio_folder.endswith('/'): audio_folder += '/'
output_folder = sys.argv[3]
if not output_folder.endswith('/'): output_folder += '/'


##################################################
#
# 1. Reading ratings file
#
print("Reading ratings file...")

file2records = {}

with open(input_file, "r") as inf:
    inf.readline()
    for cpt, line in enumerate(inf):
        line = line.strip()
        l = line.split(';')
        if len(l) != 11:
            print("NUMBER OF COLUMNS DOES NOT MATCH EXPECTATION! line", cpt)
            print(line)
        else:
            prolificPid, segment, trainingPhase, nbClicks, clicks, globalPron, globalFluency, globalComp, globalComment, timeStamp, userAgent = l
            segment = re.sub(r'(.mp3|.ogg|.wav)','',segment)

            if segment not in file2records.keys():
                file2records[segment] = []

            file2records[segment].append({
                "prolificPid":prolificPid,
                "segment":segment,
                "trainingPhase":trainingPhase,
                "nbClicks":int(nbClicks),
                "clicks":json.loads(clicks),
                "globalPron":int(globalPron),
                "globalFluency":int(globalFluency),
                "globalComp":int(globalComp),
                "globalComment":globalComment,
                "timeStamp":timeStamp,
                "userAgent":userAgent
                })

print('List of audio recordings with number of ratings:')
for seg, entries in file2records.items():
    print(seg, len(entries))


##################################################
#
# 2. Creating TextGrid files
#
print("Creating TextGrid files...")

for seg, entries in file2records.items():
    
    grid = textgrids.TextGrid()

    # Get audio duration
    #print("Get audio duration of",audio_folder + seg+'.wav ...')
    if seg+".wav" not in os.listdir(audio_folder):
        continue

    with contextlib.closing(wave.open(audio_folder + seg+".wav",'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)

    grid.xmin = 0
    grid.xmax = duration

    grid["all"] = textgrids.Tier() # merge all raters' ratings

    id2cpt = {}
    allClicks = [] # all the clicks for this audio

    for entrie in sorted(entries,key=lambda t:t["prolificPid"]):
        # Allow multiple ratings for the same rater: the value will be incremented with each rating for this rater
        if str(entrie['prolificPid']) not in id2cpt.keys(): id2cpt[str(entrie['prolificPid'])] = 1

        # For each rating, create a tier with rater's name (with _2, _3... if multiple ratings for the same rater)
        prolificPid = str(entrie['prolificPid'])
        if prolificPid in grid.keys():
            tierName = prolificPid + '_' + str(id2cpt[prolificPid]+1)
        else: 
            tierName = prolificPid
        grid[tierName] = textgrids.Tier()

        print(seg, entrie["clicks"])

        for i, click in enumerate(entrie["clicks"]):
            allClicks.append(float(click))
            point = textgrids.Point(text=str(i), xpos=float(click))
            grid[tierName].append(point)

        # if len(entrie["clicks"])==0:
        #     print("NO CLICK", entrie)
        #     emptyInterval = textgrids.Interval(xmin=0, xmax=duration, text="")
        #     grid[tierName].append(emptyInterval)

    #Add all clicks in "all" tier as well
    for i, click in enumerate(sorted(allClicks)):
        point = textgrids.Point(text=i, xpos=float(click))
        grid["all"].append(point)


    # grid.write(output_folder+seg+'.TextGrid') ####  ERROR
    # Manual export:
    with open(output_folder + seg + '.TextGrid', 'w') as outf:
        outf.write('''File type = "ooTextFile"
    Object class = "TextGrid"

    xmin = 0 
    xmax = {}
    tiers? <exists> 
    size = {}
    item []:\n'''.format(duration, len(grid.keys())))

        for i,tierName in enumerate(grid.keys()):
            outf.write('''    item [{}]:
                class = "TextTier"
                name = "{}"
                xmin = 0
                xmax = {}
                intervals: size = {}\n'''.format(i, tierName, duration, len(grid[tierName])))
            for j,click in enumerate(grid[tierName]):
                outf.write('''        point [{}]:
                    xpos = {}
                    text = "{}"\n'''.format(j+1,click.xpos, click.text))

    print(output_folder+seg+'.TextGrid')

print("Done.")


'''
# Get audio duration
    print("Get audio duration of",audio_folder + seg+'.wav ...')
    with contextlib.closing(wave.open(audio_folder + seg+'.wav','r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
        print(duration)

### CHANGER LA CLASSE PointTier en TextTier (nouvelle syntaxe de Praat)
with open(output_folder+ outFile, 'r') as outf:
    fifi = outf.read()
    fifi = fifi.replace('class = "PointTier"','class = "TextTier"')

with open(output_folder+ outFile, 'w') as outf:
    outf.write(fifi)

'''
