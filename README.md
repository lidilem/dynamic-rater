![DynamicRaterLogo](static/banner.png){width=500px}

The Dynamic Rater is a server-based application designed for dynamic rating of speech comprehensibility while listening to audio recordings. Raters are asked to click on a button each time -- and as soon as -- they feel they are struggling to understand the speaker. This protocol aims to identify regions of low comprehensibility in speech and help investigating the potential causes of it.

## Inside this repository

This repository contains the whole software to be installed on your computer or server (cf. installation section below), as well as the directory `data-analysis/` containing scripts for data processing and data files used in the InterSpeech paper below.

## Installation of Dynamic Rater

```shell
# 1. Clone this repository
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lidilem/dynamic-rater.git
cd dynamic-rater

# 2. Create a virtual environment and install the required libraries from `REQUIREMENTS.txt`
pipenv shell
pip install -r REQUIREMENTS.txt

# 3. Create empty directories for audio files and output files
mkdir output static/audio static/audioTraining
# Then add the audio files you want your participants rate in `static/audio/`, and add one training audio file in `static/audioTraining/` (ratings for this file will still be available in the output file, but tagged as "training")

# 4. Launch the Django server
python manage.py runserver
```

At this point, you should be able to access to the application locally on your browser by accessing to this url: http://127.0.0.1:8000/

If it does not work, check if Django and other libraries were correctly installed.

## Rating tool usage

You can put as many audio files as you want in the `audio` directory, and set a number of files to be randomly presented to the participants in `raterapp/views.py` line 15 (by default all the files in `audio/`):
```python
numberOfSegments = 16 # in the case you want to randomly choose 16 audio files among files from the audio/ directory
```

Results are saved in `output/` each time a rater click on "Continue" or "Next", so you may get partial data if the rater does not go until the end of the experiment. The output files are as follow:
- `questionnaires.csv` containing the following columns: prolificPid (ID from Prolific if the rater is recruted on Prolific, else free ID), mother tongue,country, learnt languages, timeStamp (starting time of the experiment, once the questionnaire is submitted), userAgent (technical details about device and browser used);
- `dynamicRatings.csv` containing the clicks timestamps and global ratings, with the following columns: prolificPid, segment (audio file), trainingPhase (True if training audio, else False), number of clicks, list of click timestamps, global pronunciation rating, global fluency rating, global comprehensibility rating, global comment, timeStamp, userAgent;
- `finalComment.csv` containing the final comment of the participant.

## Data analysis
Check out the readme file in `data-analysis`.

## Support
If you need any help or if you have any question, feel free to [contact me](mailto:sylvain.coulange@univ-grenoble-alpes.fr).


## License
![CCBYSA_logo](https://licensebuttons.net/l/by-sa/4.0/88x31.png) This pipeline is an open-source work under the licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Feel free to use and adapt it to your needs!

## Citation
Please cite the paper below if you use or get inspiration from this tool:

[Coulange, S., Kato, T., Rossato, R., Masperi, M. (2024). Exploring Impact of Pausing and Lexical Stress Patterns on L2 English Comprehensibility in Real Time. Proceedings of Interspeech 2024.](https://www.isca-archive.org/interspeech_2024/coulange24_interspeech.html)

This study was also presented at EPIP8 (English Pronunciation: Issues and Practices):

Coulange, S., Kato, T., Rossato, R., Masperi, M. (2024). Dynamic Approach to Comprehensibility Assessment in Foreign Language Pronunciation Training. 8th International Conference on English Pronunciation: Issues and Practices, May 2024, Santander, Spain. [ABSTRACT](http://i3l.univ-grenoble-alpes.fr/~coulangs/doc/CoulangeAl2024_EPIP8.pdf)

## Acknowledgments

This tool is strongly inspired by the Idiodynamic Software (Peter MacIntyre, Cape Breton University, Canada)
https://petermacintyre.weebly.com/idiodynamic-software.html and its online version made by 80113hunterhsu (https://github.com/80113hunterhsu/VariableTester).