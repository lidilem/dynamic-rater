asgiref==3.7.2
Django==4.2.6
PyYAML==6.0.1
sqlparse==0.4.4
typing_extensions==4.8.0
ua-parser==0.18.0
user-agents==2.2.0
