// Get PROLIFIC_PID from URL
const urlParams = new URLSearchParams(window.location.search);
var prolificPid = urlParams.get('PROLIFIC_PID')
if (prolificPid) {
    console.log('Prolific ID detected:',prolificPid)
} else {
    console.log('No prolific ID')
}
document.getElementById('pid').innerText = "Current ID: "+prolificPid


var motherTongue = ""
var country = ""
var learntLang = ""

function checkQrAnswers() {
    motherTongue = document.getElementById('motherTongue').value;
    country = document.getElementById('country').value;
    learntLang = document.getElementById('question1').value;
    console.log(motherTongue, country, learntLang)

    var key = true;
    if (motherTongue.length == 0) {
        document.getElementById('motherTongue').style.borderColor = "red"
        key = false;
    }

    if (country=="") {
        document.getElementById('country').style.borderColor = "red"
        key = false;
    }

    if (learntLang.length==0) {
        document.getElementById('question1').style.borderColor = "red"
        key = false;
    }

    if (key) {
        sendQrAnswers()
    }
}

function initField(el) {
    el.style.borderColor = "";
}

async function sendQrAnswers() {
    var colis = {
        prolificPid,
        motherTongue,
        country,
        learntLang
    }

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // REQUÊTAGE SERVEUR
	console.log('Sending data...');
	const response = await fetch('/_getQrAnswers/', options)
    const data = await response.json();
	console.log(data);
    
    if (data["rep"]=="OK") {
        window.location = "/exp?PROLIFIC_PID="+prolificPid
    }
}