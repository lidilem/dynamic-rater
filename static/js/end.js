// Get PROLIFIC_PID from URL
const urlParams = new URLSearchParams(window.location.search);
var prolificPid = urlParams.get('PROLIFIC_PID')
if (prolificPid) {
    console.log('Prolific ID detected:',prolificPid)
} else {
    console.log('No prolific ID')
}
document.getElementById('pid').innerText = "Current ID: "+prolificPid



async function validateExperiment() {
    var comment = document.getElementById('optcomment').value;
    console.log(comment);

    var colis = {
        prolificPid,
        comment,
    }

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // REQUÊTAGE SERVEUR
	console.log('Sending data...');
	const response = await fetch('/_validateExperiment/', options)
    const data = await response.json();
	console.log(data);
    
    if (data["rep"]=="OK") {
        window.location = data["completionUrl"]
        document.getElementById('manualUrl').innerHTML = `You should be automatically redirected to Prolific app. If it does not word, click on the following url: <a href='${data["completionUrl"]}'>${data["completionUrl"]}</a>`
    }
}