const check = `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="green" class="bi bi-check-circle-fill ms-2" viewBox="0 0 16 16">
<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0m-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
</svg>`

// Get PROLIFIC_PID from URL
const urlParams = new URLSearchParams(window.location.search);
var prolificPid = urlParams.get('PROLIFIC_PID')
if (prolificPid) {
    console.log('Prolific ID detected:',prolificPid)
    var idMessage = "Your Prolific user ID is: "+prolificPid+check;
    document.getElementById('startBtn').disabled = false;
} else {
    console.log('No prolific ID')
    var idMessage = "No Prolific user ID has been detected in the URL. Please fill in manually your Prolific ID below:"
    document.getElementById('manualIdDiv').style.display = "block";
}
document.getElementById('idMessage').innerHTML = idMessage

function validateManualId() {
    prolificPid = document.getElementById('manualId').value;
    if (prolificPid.length>0) {
        document.getElementById('manualIdDiv').style.display = "none";
        idMessage = "Your Prolific user ID is: "+prolificPid+check;
        document.getElementById('idMessage').innerHTML = idMessage
        document.getElementById('startBtn').disabled = false;
    }
}


function loadQrPage() {
    window.location = "/qr?PROLIFIC_PID="+prolificPid
}