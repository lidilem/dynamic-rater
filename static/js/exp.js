// Get PROLIFIC_PID from URL
const urlParams = new URLSearchParams(window.location.search);
var prolificPid = urlParams.get('PROLIFIC_PID')
if (prolificPid) {
    console.log('Prolific ID detected:',prolificPid)
} else {
    console.log('No prolific ID')
    window.location.replace("/");
}
document.getElementById('pid').innerText = "Current ID: "+prolificPid

var wavesurfer = null;
var wsRegions = null;

var currentTime = 0;
var clicks = [];

// INIT THE PAGE WITH FIRST TRAINING AUDIO
var trainingPhase = true
var currentSegment = 0
var currentSegmentName = trainingSegments[currentSegment]
loadNewSegment(trainingSegmentsFolder + trainingSegments[currentSegment])



// Run this function to load a new audio segment
// More info here: https://wavesurfer.xyz/examples/?events.js
function loadNewSegment(filePath) {
    // Initialization
    document.getElementById('playMask').style.display = "block";
    document.getElementById('waveform').innerHTML = "";
    initGlobalRating()
    currentTime = 0;
    clicks = [];

    wavesurfer = WaveSurfer.create({
        container: '#waveform',
        waveColor: '#4F4A85',
        progressColor: '#383351',
        interact: false,
        url: filePath
        // url: '/static/audio/informationENG_ooOo.mp3'
    })
    // Initialize the Regions plugin
    wsRegions = wavesurfer.registerPlugin(WaveSurfer.Regions.create())
    
    // Initialize the Timeline plugin
    wavesurfer.registerPlugin(WaveSurfer.Timeline.create())
    
    /** When audio starts loading */
    wavesurfer.on('load', (url) => {
        console.log('Load', url)
    })
    
    
    /** During audio loading */
    wavesurfer.on('loading', (percent) => {
        document.getElementById('startBtn').innerText = 'Loading '+ percent + '%'
    })
    
    
    /** When the audio is both decoded and can play */
    wavesurfer.on('ready', (duration) => {
        console.log('Ready. Total duration of audio:', duration + 's')
        document.getElementById('startBtn').innerText = "Start"
        document.getElementById('startBtn').disabled = false
        document.getElementById('startBtn').classList.add('btn-primary')
    })
    
    /** When the audio starts playing */
    wavesurfer.on('play', () => {
        console.log('Play')
    })
    
    /** When the audio pauses */
    wavesurfer.on('pause', () => {
        console.log('Pause')
    })
      
    /** When the audio finishes playing */
    wavesurfer.on('finish', () => {
        console.log('Finish')
        console.table(clicks)
    
        document.getElementById('dynamicRating').style.display = "none";
        document.getElementById('globalRating').style.display = "block";
    })

    /** On audio position change, fires continuously during playback */
    wavesurfer.on('timeupdate', (ct) => {
        currentTime = ct
    })
}


function struggling() {
    if (currentTime>0) {
        console.log('Struggling', currentTime + 's')
        wsRegions.addRegion({
            start: currentTime,
            color: "#000000",
            drag: false,
        })
        clicks.push(currentTime)
    }
}

function validateQuestionnaire() {
    // Get list of clicks, answers to the questionnaire and send them to the server. Then load next audio.
    var colis = {
        'prolificPid': prolificPid,
        'segment': currentSegmentName,
        'trainingPhase': trainingPhase,
        'clicks': clicks,
        "globalPron": document.getElementById('globalPron').value,
        "globalFluency": document.getElementById('globalFluency').value,
        "globalComp": document.getElementById('globalComp').value,
        "globalComment": document.getElementById('globalComment').value
    }
    console.table(colis)
    sendDynamicRating(colis)
}

async function sendDynamicRating(colis) {
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // REQUÊTAGE SERVEUR
	console.log('Sending data...');
	const response = await fetch('/_getDynamicRating/', options)
    const data = await response.json();
	console.log(data);

    if (clicks.length<=2) {
        if(window.confirm("Do not hesitate to click more often while listening.")) {
            loadNext();
        }
    } else {
        loadNext();
    }
}

function loadNext() {
    if (trainingPhase) {
        trainingPhase = false;
        currentSegment = 0
        document.getElementById('trainingPhaseDiv').style.display = "none"
        document.getElementById('totalSegmentsNb').innerText = segments.length
        document.getElementById('experimentPhaseDiv').style.display = "block"
    } else {
        currentSegment++
    }
    
    currentSegmentName = segments[currentSegment]
    document.getElementById('currentSegmentNb').innerText = currentSegment+1

    if (currentSegment==segments.length) {
        window.location = "/end?PROLIFIC_PID="+prolificPid
    } else {
        loadNewSegment(segmentsFolder + segments[currentSegment])
    }
}


function initGlobalRating() {
    document.getElementById('globalPron').value = 50
    document.getElementById('globalFluency').value = 50
    document.getElementById('globalComp').value = 50
    document.getElementById('globalComment').value = ""

    document.getElementById('dynamicRating').style.display = "block";
    document.getElementById('globalRating').style.display = "none";
}